# CME Lab Utilies

Helpful functions used by the [CME Lab](https://www.boisestate.edu/coen-cmsl).

### Installation
Installation of CME Lab Utilities requires the conda package manager. We recommend [Miniconda](https://docs.conda.io/en/latest/miniconda.html).
1. Clone this repository:
```
git clone git@gitlab.com:bsu/cme-lab/cme_lab_utils.git
cd cme_lab_utils
```
2. Set up and activate environment:
```
conda env create -f environment.yml
```
3. Install this package with pip:
```
pip install .
```
